from unittest import mock
from freezegun import freeze_time
from textwrap import dedent

from custom_unit_test_class import PrintingUnitTestCase

import ufterm as uft


_mock_container = {}


def _build_side_effect_mock(name: str, *args):
    new_mock = mock.Mock()
    new_mock.side_effect = args
    _mock_container[name] = new_mock


def command1():
    uft.show("cmd1: information message")
    uft.show("cmd1: error message", uft.ERROR_MSG)


def command2():
    uft.show("cmd2: information message")
    uft.show("cmd2: warning message", uft.WARNING_MSG)
    uft.show("cmd2: error message", uft.ERROR_MSG)
    uft.done()


class InitTest(PrintingUnitTestCase):

    def test_enumerate_input_def(self):
        form_def = (("str_Input", str, "d-value"),
                    ("Integer_input", int),
                    ("boolean_input", bool),
                    ("Float_input", float),
                    ("Float_input", float, .85),
                    ("boolean_input", bool),
                    ("boolean_input", bool),
                    ("boolean_input", bool),
                    ("boolean_input", bool, True),
                    ("boolean_input", bool, False),
                    ("str_defaulted_input", str, "Hello", lambda s: not uft.utils.is_empty_str(s)))
        generator = uft._enumerate_input_def(form_def, "my_id")

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("str_Input", label)
        self.assertEqual("my_id_input_01", input_id)
        self.assertEqual(str, type_)
        self.assertEqual("d-value", default)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("Integer_input", label)
        self.assertEqual(int, type_)
        self.assertIsNone(default)
        self.assertEqual("my_id_input_02", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("boolean_input", label)
        self.assertEqual(bool, type_)
        self.assertIsNone(default)
        self.assertEqual("my_id_input_03", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("Float_input", label)
        self.assertEqual(float, type_)
        self.assertIsNone(default)
        self.assertEqual("my_id_input_04", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("Float_input", label)
        self.assertEqual(float, type_)
        self.assertEqual(0.85, default)
        self.assertEqual("my_id_input_05", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("boolean_input", label)
        self.assertEqual(bool, type_)
        self.assertIsNone(default)
        self.assertEqual("my_id_input_06", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("boolean_input", label)
        self.assertEqual(bool, type_)
        self.assertIsNone(default)
        self.assertEqual("my_id_input_07", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("boolean_input", label)
        self.assertEqual(bool, type_)
        self.assertIsNone(default)
        self.assertEqual("my_id_input_08", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("boolean_input", label)
        self.assertEqual(bool, type_)
        self.assertTrue(default)
        self.assertEqual("my_id_input_09", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("boolean_input", label)
        self.assertEqual(bool, type_)
        self.assertFalse(default)
        self.assertEqual("my_id_input_10", input_id)
        self.assertTrue(validator(""))

        label, input_id, type_, default, validator = next(generator)
        self.assertEqual("str_defaulted_input", label)
        self.assertEqual(str, type_)
        self.assertEqual(default, "Hello")
        self.assertEqual("my_id_input_11", input_id)
        self.assertFalse(validator(""))
        self.assertFalse(validator("  "))
        self.assertTrue(validator(default))


class InitScenarioTest(PrintingUnitTestCase):

    def setUp(self):
        super().setUp()
        uft.reset()

    def test_done(self):
        self.assertFalse(uft.is_done())
        self.assertFalse(uft.is_done())
        uft.done()
        self.assertTrue(uft.is_done())

    def test_add_command(self):
        uft.add_command(command1)
        self.assertRaises(uft.CommandAlreadyPresentError, lambda: uft.add_command(command1))

    def test_add_command_with_custom_name(self):
        uft.add_command(command1, "Custom name")
        self.assertRaises(uft.CommandAlreadyPresentError, lambda: uft.add_command(command1))

    def test_add_command_with_same_custom_name(self):
        uft.add_command(command1, "Custom Name")
        self.assertRaises(uft.CommandNameAlreadyUsedError, lambda: uft.add_command(command2, "Custom Name"))

    @freeze_time('2175-08-15 18:42:27')
    def test_run_command(self):
        uft.add_command(command1)
        expected_output = ("[18:42:27] cmd1: information message", "[18:42:27] cmd1: error message")
        self.assertPrintEquals(expected_output, lambda: uft.run_command())


_build_side_effect_mock('simple_loop', "0", "exit")
_build_side_effect_mock('second_loop', "0", "0", "1", "exit")
_build_side_effect_mock('direct_exit_loop', "exit")


class InitTestLoop(PrintingUnitTestCase):

    def setUp(self):
        super().setUp()
        uft.reset()
        uft.add_command(command1)
        uft.add_command(command2)

    @freeze_time('2175-08-15 18:42:27')
    @mock.patch('builtins.input', lambda _: _mock_container['simple_loop']())
    def test_simple_loop(self):
        out_expected = dedent("""
            [  0]: command1
            [  1]: command2
            [18:42:27] cmd1: information message
            [  0]: command1
            [  1]: command2""").strip()
        err_expected = "[18:42:27] cmd1: error message"
        self.assertPrintEquals((out_expected, err_expected), uft.loop)

    @freeze_time('2175-08-15 18:42:27')
    @mock.patch('builtins.input', lambda _: _mock_container['second_loop']())
    def test_second_loop(self):
        out_expected = dedent("""
            [  0]: command1
            [  1]: command2
            [18:42:27] cmd1: information message
            [  0]: command1
            [  1]: command2
            [18:42:27] cmd1: information message
            [  0]: command1
            [  1]: command2
            [18:42:27] cmd2: information message""").strip()
        err_expected = dedent("""
            [18:42:27] cmd1: error message
            [18:42:27] cmd1: error message
            [18:42:27] cmd2: warning message
            [18:42:27] cmd2: error message""").strip()
        self.assertPrintEquals((out_expected, err_expected), uft.loop)

    @freeze_time('2175-08-15 18:42:27')
    @mock.patch('builtins.input', lambda _: _mock_container['direct_exit_loop']())
    def test_direct_exit_loop(self):
        out_expected = dedent("""
            [  0]: command1
            [  1]: command2
            """).strip()
        self.assertPrintEquals((out_expected, ""), uft.loop)

    @freeze_time('2175-08-15 18:42:27')
    def test_run_command_assert(self):
        self.assertRaises(uft.NoCommandSelectedError, lambda: uft.run_command())

