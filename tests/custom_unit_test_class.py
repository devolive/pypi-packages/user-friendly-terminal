import unittest
import sys

from io import StringIO
from typing import Tuple
from contextlib import contextmanager


class PrintingUnitTestCase(unittest.TestCase):

    def assertPrintEquals(self, expected: Tuple[str, str], function: callable, msg: str = None):
        with self.captured_output() as (out, err):
            function()
        self.assertEqual(expected[0], out.getvalue().strip(), msg)
        self.assertEqual(expected[1], err.getvalue().strip(), msg)

    @contextmanager
    def captured_output(self):
        new_out, new_err = StringIO(), StringIO()
        old_out, old_err = sys.stdout, sys.stderr
        try:
            sys.stdout, sys.stderr = new_out, new_err
            yield sys.stdout, sys.stderr
        finally:
            sys.stdout, sys.stderr = old_out, old_err
