# User Friendly Terminal
Simple module help user to use terminal script.  
The library allows to see a Graphical Interface instead of a terminal.


## Code example
```python
import ufterm as uft

def command1():
    ...

def command2():
    ...

if __name__ == '__main__':
    uft.add_command(command1)
    uft.add_command(command2)
    uft.loop()
```
## Issues/Bug report or improvement ideas
https://gitlab.com/olive007/user-friendly-terminal/-/issues


## License
GNU Lesser General Public License v3 or later (LGPLv3+)
